package com.gx.rbac.springsecurityrbac.service.impl;

import com.alibaba.fastjson.JSON;
import com.gx.rbac.springsecurityrbac.entity.User;
import com.gx.rbac.springsecurityrbac.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by GX on 2017/8/28.
 */
@SpringBootTest
@Slf4j
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Autowired
    UserMapper userMapper;
    @Test
    public void loadUserByUsername() throws Exception {
        User user = userMapper.findByUsername("admin");
        System.out.println(JSON.toJSONString(user));
    }

}