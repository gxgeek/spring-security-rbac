package com.gx.rbac.springsecurityrbac;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GX on 2017/8/25.
 */
public class AutoCode {

//    public static void main(String[] args) {
//        // 自定义需要填充的字段
//        List<TableFill> tableFillList = new ArrayList<>();
//        tableFillList.add(new TableFill("ASDD_SS", FieldFill.INSERT_UPDATE));
//
//        // 代码生成器
//        AutoGenerator mpg = new AutoGenerator().setGlobalConfig(
//                // 全局配置
//                new GlobalConfig()
//                        .setOutputDir("F:\\develop\\code\\")//输出目录
//                        .setFileOverride(true)// 是否覆盖文件
//                        .setActiveRecord(true)// 开启 activeRecord 模式
//                        .setEnableCache(false)// XML 二级缓存
//                        .setBaseResultMap(true)// XML ResultMap
//                        .setBaseColumnList(true)// XML columList
//                        .setAuthor("GX")
//                // 自定义文件命名，注意 %s 会自动填充表实体属性！
//                // .setMapperName("%sDao")
//                // .setXmlName("%sDao")
//                // .setServiceName("MP%sService")
//                // .setServiceImplName("%sServiceDiy")
//                // .setControllerName("%sAction")
//        ).setDataSource(
//                // 数据源配置
//                new DataSourceConfig()
//                        .setDbType(DbType.MYSQL)// 数据库类型
//                        .setTypeConvert(new MySqlTypeConvert() {
//                            // 自定义数据库表字段类型转换【可选】
//                            @Override
//                            public DbColumnType processTypeConvert(String fieldType) {
//                                System.out.println("转换类型：" + fieldType);
//                                // if ( fieldType.toLowerCase().contains( "tinyint" ) ) {
//                                //    return DbColumnType.BOOLEAN;
//                                // }
//                                return super.processTypeConvert(fieldType);
//                            }
//                        })
//                        .setDriverName("com.mysql.jdbc.Driver")
//                        .setUsername("root")
//                        .setPassword("123")
//                        .setUrl("jdbc:mysql://localhost:3306/hyd_sp2p_dev2?characterEncoding=utf8")
//        ).setStrategy(
//                // 策略配置
//                new StrategyConfig()
//                        // .setCapitalMode(true)// 全局大写命名
//                        // .setDbColumnUnderline(true)//全局下划线命名
//                        .setTablePrefix(new String[]{"t_"})// 此处可以修改为您的表前缀
//                        .setNaming(NamingStrategy.underline_to_camel)// 表名生成策略
//                        // .setInclude(new String[] { "user" }) // 需要生成的表
//                        // .setExclude(new String[]{"test"}) // 排除生成的表
//                        // 自定义实体父类
//                        // .setSuperEntityClass("com.baomidou.demo.TestEntity")
//                        // 自定义实体，公共字段
//                        .setSuperEntityColumns(new String[]{"test_id"})
//                        .setTableFillList(tableFillList)
//                // 自定义 mapper 父类
//                // .setSuperMapperClass("com.baomidou.demo.TestMapper")
//                // 自定义 service 父类
//                // .setSuperServiceClass("com.baomidou.demo.TestService")
//                // 自定义 service 实现类父类
//                // .setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl")
//                // 自定义 controller 父类
//                // .setSuperControllerClass("com.baomidou.demo.TestController")
//                // 【实体】是否生成字段常量（默认 false）
//                // public static final String ID = "test_id";
//                // .setEntityColumnConstant(true)
//                // 【实体】是否为构建者模型（默认 false）
//                // public User setName(String name) {this.name = name; return this;}
//                // .setEntityBuilderModel(true)
//                // 【实体】是否为lombok模型（默认 false）<a href="https://projectlombok.org/">document</a>
//                // .setEntityLombokModel(true)
//                // Boolean类型字段是否移除is前缀处理
//                // .setEntityBooleanColumnRemoveIsPrefix(true)
//                // .setRestControllerStyle(true)
//                // .setControllerMappingHyphenStyle(true)
//        ).setPackageInfo(
//                // 包配置
//                new PackageConfig()
////                        .setModuleName("test")
//                        .setParent("com.hzy.hyd")// 自定义包路径
//                        .setController("controller")// 这里是控制器包名，默认 web
//                        .setEntity("entity")// 这里是控制器包名，默认 web
//                        .setMapper("dao.mapper")// 这里是控制器包名，默认 web
////                        ("dao.mapper")// 这里是控制器包名，默认 web
//        ).setCfg(
//                // 注入自定义配置，可以在 VM 中使用 cfg.abc 设置的值
//                new InjectionConfig() {
//                    @Override
//                    public void initMap() {
//                        Map<String, Object> map = new HashMap<>();
//                        map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
//                        this.setMap(map);
//                    }
//                }.setFileOutConfigList(Collections.<FileOutConfig>singletonList(new FileOutConfig("/templates/mapper.xml.vm") {
//                    // 自定义输出文件目录
//                    @Override
//                    public String outputFile(TableInfo tableInfo) {
//                        return "/develop/code/xml/" + tableInfo.getEntityName() + ".xml";
//                    }
//                }))
//        ).setTemplate(
//                // 关闭默认 xml 生成，调整生成 至 根目录
//                new TemplateConfig().setXml(null)
//                // 自定义模板配置，模板可以参考源码 /mybatis-plus/src/main/resources/template 使用 copy
//                // 至您项目 src/main/resources/template 目录下，模板名称也可自定义如下配置：
//                // .setController("...");
//                // .setEntity("...");
//                // .setMapper("...");
//                // .setXml("...");
//                // .setService("...");
//                // .setServiceImpl("...");
//        );
//
//        // 执行生成
//        mpg.execute();
//
//        // 打印注入设置，这里演示模板里面怎么获取注入内容【可无】
//        System.err.println(mpg.getCfg().getMap().get("abc"));
//
//    }


    public static void main(String[] args) {
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
//        gc.setOutputDir("D://");
        gc.setOutputDir("F:\\develop\\code\\security-rbac");
        gc.setFileOverride(true);
        gc.setActiveRecord(true);
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        gc.setAuthor("GX");

        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        // gc.setMapperName("%sDao");
        // gc.setXmlName("%sDao");
        // gc.setServiceName("MP%sService");
        // gc.setServiceImplName("%sServiceDiy");
        // gc.setControllerName("%sAction");
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setTypeConvert(new MySqlTypeConvert(){
            // 自定义数据库表字段类型转换【可选】
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                System.out.println("转换类型：" + fieldType);
                // 注意！！processTypeConvert 存在默认类型转换，如果不是你要的效果请自定义返回、非如下直接返回。
                return super.processTypeConvert(fieldType);
            }
        });
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123");
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf8");
        mpg.setDataSource(dsc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // strategy.setCapitalMode(true);// 全局大写命名 ORACLE 注意
        strategy.setTablePrefix(new String[] { "sys_" });// 此处可以修改为您的表前缀
        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
        // strategy.setInclude(new String[] { "user" }); // 需要生成的表
        // strategy.setExclude(new String[]{"test"}); // 排除生成的表
        // 自定义实体父类
        // strategy.setSuperEntityClass("com.baomidou.demo.TestEntity");
        // 自定义实体，公共字段
        // strategy.setSuperEntityColumns(new String[] { "test_id", "age" });
        // 自定义 mapper 父类
        // strategy.setSuperMapperClass("com.baomidou.demo.TestMapper");
        // 自定义 service 父类
        // strategy.setSuperServiceClass("com.baomidou.demo.TestService");
        // 自定义 service 实现类父类
        // strategy.setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl");
        // 自定义 controller 父类
        // strategy.setSuperControllerClass("com.baomidou.demo.TestController");
        // 【实体】是否生成字段常量（默认 false）
        // public static final String ID = "test_id";
        // strategy.setEntityColumnConstant(true);
        // 【实体】是否为构建者模型（默认 false）
        // public User setName(String name) {this.name = name; return this;}
        // strategy.setEntityBuilderModel(true);
        mpg.setStrategy(strategy);

        // 包配置
        PackageConfig pc = new PackageConfig();
//        pc.setParent("com.baomidou");
//        pc.setModuleName("test")
        pc .setParent("com.gx.rbac.springsecurityrbac")// 自定义包路径
           .setController("controller")// 这里是控制器包名，默认 web
           .setEntity("entity")// 这里是控制器包名，默认 web
          .setMapper("mapper")// 这里是控制器包名，默认 web
        ;
        mpg.setPackageInfo(pc);

        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                this.setMap(map);
            }
        };

        // 自定义 xxList.jsp 生成
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
//        focList.add(new FileOutConfig("/template/list.jsp.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输入文件名称
//                return "D://my_" + tableInfo.getEntityName() + ".jsp";
//            }
//        });
//        cfg.setFileOutConfigList(focList);
//        mpg.setCfg(cfg);

        // 调整 xml 生成目录演示
        focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return "/develop/code/xml/" + tableInfo.getEntityName() + ".xml";
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 关闭默认 xml 生成，调整生成 至 根目录
//        TemplateConfig tc = new TemplateConfig();
//        tc.setXml(null);
//        mpg.setTemplate(tc);

//         自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
//         放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
//         TemplateConfig tc = new TemplateConfig();
//         tc.setController("...");
//         tc.setEntity("...");
//         tc.setMapper("...");
//         tc.setXml("...");
//         tc.setService("...");
//         tc.setServiceImpl("...");
//////         如上任何一个模块如果设置 空 OR Null 将不生成该模块。
//         mpg.setTemplate(tc);

        // 执行生成
        mpg.execute();

        // 打印注入设置【可无】
//        System.err.println(mpg.getCfg().getMap().get("abc"));
    }


}
