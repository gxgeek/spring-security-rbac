package com.gx.rbac.springsecurityrbac.service.impl;

import com.gx.rbac.springsecurityrbac.mapper.UserMapper;
import com.gx.rbac.springsecurityrbac.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by GX on 2017/8/28.
 */
@Service
public class UserServiceImpl implements UserService ,UserDetailsService {

    @Autowired
    private UserMapper userMapper;


    //重写loadUserByUsername 方法获得 userdetails 类型用户
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userMapper.findByUsername(username);
    }

}
