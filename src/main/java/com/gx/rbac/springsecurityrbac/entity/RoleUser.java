package com.gx.rbac.springsecurityrbac.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户角色对应表
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
@Data
@TableName("sys_role_user")
public class RoleUser extends Model<RoleUser> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	@TableField("user_id")
	private Long userId;
	@TableField("role_id")
	private Long roleId;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
