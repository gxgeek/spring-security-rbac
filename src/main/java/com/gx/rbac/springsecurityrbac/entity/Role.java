package com.gx.rbac.springsecurityrbac.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
@Data
@TableName("sys_role")
public class Role extends Model<Role> implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 角色名称
     */
	private String name;
	// 角色对应的 权限列表
	private List<Permission> permissions;



	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String getAuthority() {
		return name;
	}
}
