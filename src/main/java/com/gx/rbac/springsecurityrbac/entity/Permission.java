package com.gx.rbac.springsecurityrbac.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
@Data
@TableName("sys_permission")
public class Permission extends Model<Permission>   {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 权限名称
     */
	private String name;
    /**
     * 权限描述
     */
	private String description;
    /**
     * 权限对应的URL
     */
	private String url;
    /**
     * 父节点id
     */
	private Long pid;
    /**
     * 权限对应请求方式 适应 Restful 风格  0 全部权限  1 get； 2 post； 3 delete ；4 put 
     */
	@TableField("request_method")
	private Integer requestMethod;



	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
