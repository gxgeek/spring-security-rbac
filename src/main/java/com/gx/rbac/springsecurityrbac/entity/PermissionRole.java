package com.gx.rbac.springsecurityrbac.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 角色权限对应表
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
@Data
@TableName("sys_permission_role")
public class PermissionRole extends Model<PermissionRole> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	@TableField("role_id")
	private Long roleId;
	@TableField("permission_id")
	private Long permissionId;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
