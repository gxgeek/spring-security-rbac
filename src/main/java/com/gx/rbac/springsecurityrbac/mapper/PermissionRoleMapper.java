package com.gx.rbac.springsecurityrbac.mapper;

import com.gx.rbac.springsecurityrbac.entity.PermissionRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 角色权限对应表 Mapper 接口
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
public interface PermissionRoleMapper extends BaseMapper<PermissionRole> {

}