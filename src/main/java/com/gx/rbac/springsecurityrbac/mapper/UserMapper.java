package com.gx.rbac.springsecurityrbac.mapper;

import com.gx.rbac.springsecurityrbac.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * <p>
  * 用户表 Mapper 接口
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
public interface UserMapper extends BaseMapper<User> {

    User findByUsername(String username);
}