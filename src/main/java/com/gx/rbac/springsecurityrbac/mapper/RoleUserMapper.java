package com.gx.rbac.springsecurityrbac.mapper;

import com.gx.rbac.springsecurityrbac.entity.RoleUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 用户角色对应表 Mapper 接口
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
public interface RoleUserMapper extends BaseMapper<RoleUser> {

}