package com.gx.rbac.springsecurityrbac.mapper;

import com.gx.rbac.springsecurityrbac.entity.Permission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
  * 权限表 Mapper 接口
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
public interface PermissionMapper extends BaseMapper<Permission> {

//     List<Permission> findAll();


    /**
     * 加载当前对象的权限  查询五张表
     * @param userId
     * @return
     */
     List<Permission> findByAdminUserId(int userId);

}