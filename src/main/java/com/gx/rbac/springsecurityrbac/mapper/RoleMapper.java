package com.gx.rbac.springsecurityrbac.mapper;

import com.gx.rbac.springsecurityrbac.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 角色表 Mapper 接口
 * </p>
 *
 * @author GX
 * @since 2017-08-28
 */
public interface RoleMapper extends BaseMapper<Role> {

}