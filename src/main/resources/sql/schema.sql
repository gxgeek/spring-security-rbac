DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`(
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT  COMMENT 'id',
  `username` VARCHAR(50) NOT NULL COMMENT '用户名',
  `password` VARCHAR(100) NOT NULL COMMENT '密码  加密' ,
	`nice_name` VARCHAR(50) NOT NULL DEFAULT '新用户' COMMENT '昵称',
	`avatar`  VARCHAR(100) NOT NULL DEFAULT 'http://www.gxgeek.xin/images/weixin/weixin.jpg' COMMENT '头像',
	`create_time` timestamp not null default current_timestamp COMMENT '注册时间',
	`last_time` timestamp not null default current_timestamp on update current_timestamp COMMENT '最后上线时间',
	`last_open_ip` VARCHAR(30) NOT NULL DEFAULT '无记录' COMMENT '最后上线IP地址',
	`status` tinyint(4) NOT NULL DEFAULT  1  COMMENT '用户状态  0 正常  1 封号 ',
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_username` (`username`) USING BTREE COMMENT '用户账号唯一'
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT '用户表';

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`(
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
`name` VARCHAR(200) NOT NULL  COMMENT '角色名称' ,
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT '角色表';

DROP TABLE IF EXISTS `sys_permission`;


CREATE TABLE `sys_permission`(
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL COMMENT '权限名称',
  `description` VARCHAR(200) DEFAULT NULL COMMENT '权限描述',
  `url` VARCHAR(200) NOT NULL  COMMENT '权限对应的URL' ,
  `pid` BIGINT DEFAULT NULL COMMENT '父节点id',
	 request_method tinyint(4)  NOT NULL DEFAULT  1  COMMENT '权限对应请求方式 适应 Restful 风格  0 全部权限  1 get； 2 post； 3 delete ；4 put ',
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT '权限表' ;


DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user`(
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `role_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT '用户角色对应表';


DROP TABLE IF EXISTS `sys_permission_role`;
CREATE TABLE `sys_permission_role`(
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` BIGINT UNSIGNED NOT NULL,
  `permission_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci  COMMENT '角色权限对应表';






insert into sys_user (id,username, password) values (1,'admin', '123');
insert into sys_user (id,username, password) values (2,'user', '123');

insert into sys_role(id,name) values(1,'ROLE_ADMIN');
insert into sys_role(id,name) values(2,'ROLE_USER');

insert into sys_role_user(user_id,role_id) values(1,1);
insert into sys_role_user(user_id,role_id) values(2,2);

INSERT INTO `sys_permission` VALUES ('1', 'ROLE_HOME', 'home', '/', null,0), ('2', 'ROLE_ADMIN', 'ABel', '/admin', null,0);
INSERT INTO `sys_permission_role` VALUES ('1', '1', '1'), ('2', '1', '2'), ('3', '2', '1');